import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

import com.JFly.Tools.SocketHelper;


public class StartJFlyServer {
	public static void main(String[] args) throws IOException{
		ServerSocket ss = new ServerSocket(8080);
		while(true){
			System.out.println("Wait new connection...");
			Socket receiver = ss.accept();
			System.out.println("From " + receiver.getRemoteSocketAddress().toString() + ":");
			System.out.println(SocketHelper.readString(receiver));
		}
	}
}
