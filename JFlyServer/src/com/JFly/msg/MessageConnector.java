package com.JFly.msg;

import java.net.Socket;

public  class MessageConnector {
	Socket socket;
	String name;
	public MessageConnector(Socket socket, String name) {
		this.socket = socket;
		this.name = name;
	}
	
	public Socket getSocket(){
		return this.socket;
	}
	
	public String getName(){
		return this.name;
	}
}
