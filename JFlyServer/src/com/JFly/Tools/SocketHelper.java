package com.JFly.Tools;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

public class SocketHelper {
	private final static String DEFAULE_CHARSET = "UTF-8";
	//IO资源未清空
	public static String readString(Socket s) throws IOException{
		if(s == null) throw new NullPointerException();
		InputStream i = s.getInputStream();
		if(i.available() == 0) throw new IllegalArgumentException("Message is can't read data");
		byte[] b_string = new byte[i.available()];
		String message = new String(b_string,DEFAULE_CHARSET);
		return message;
	}
	//socket,IO资源未清空
	public static void sendString(Socket s, String message) throws IOException{
		if(s == null) throw new NullPointerException();
		OutputStream o = s.getOutputStream();
		byte[] b_string = message.getBytes(DEFAULE_CHARSET);
		o.write(b_string);
	}
}
